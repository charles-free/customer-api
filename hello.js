const express = require('express');
const router = express.Router();
const app = express();
app.use(express.json());
const MyCustomer = require('./customer');

commands_list = {
    "/show_all": "Shows all customers",
    "/get_customer/<id>": "Shows cust by id",
    "/save_cust": "Saves customer"
}


customers = []

router.get('/', (req,res) => {
    res.redirect('/home')
});

router.get('/home', (req, res) => {
    res.json(commands_list);
});

router.get('/show_all', (req, res) => {
    res.json(customers);
});

router.post('/save_cust', (req, res) => {
    _name = req.body.name;
    _cust_id = customers.length;
    _order_id = req.body.order_id;
    _description = req.body.description;
    new_cust = new MyCustomer(_cust_id, _name, _order_id, _description);
    customers.push(new_cust);
    res.redirect('/show_all');
});

router.get('/get_customer/:cust_id', (req, res) => {
    req_cust_id = req.params["cust_id"];
    customers.forEach(element => {
        if (element.cust_id === req_cust_id){
            res.json(element);
        } 
    });    
    res.send(`No customer with ${req_cust_id} id`);
});

app.use('/', router);
app.listen(3000);