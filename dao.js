db_name = require('./credentials');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');


function connect_db(db_name) {
    let db = new sqlite3.Database(db_name, (err) => {
        if (err) {
          console.error(err.message);
        }
        else{
            console.log(`Connected to the ${db_name} database.`);      
        }
    });
      return db;
}

function select_all() {
    db = connect_db(db_name);
    let sql = "SELECT * FROM customer;" 
    db.all(sql, (err, rows) => {
        if (err) {
            throw err;
        }
        else{
          console.log(rows);
          fs.writeFile("temp.txt", JSON.stringify(rows),(err)=>{
              if(err){
                console.log(err);
              }
              console.log("successfully saved data in file");
          });
      }
    });
    db.close((err) => {
        if (err) {
          return console.error(err.message);
        }
        console.log('Close the database connection.');
      });
}

function insert_value(par_value) {
    db = connect_db(db_name);
    let placeholders = '(' + par_value.map((element) => '?').join(',') + ')';
    
    let sql = "INSERT INTO customer(name, order_id, description) VALUES" + placeholders;
    db.run(sql, par_value, (err)=>{
        if (err) {
            return console.error(err.message);
        } else {
            console.log(`Rows inserted successfully`);
        }
    });
    db.close((err) => {
        if (err) {
          return console.error(err.message);
        }
        console.log('Close the database connection.');
      });
}

function get_value(order_id, par_res) {
  db = connect_db(db_name);
  sql = 'select * from customer where order_id = ?';
  db.get(sql, [order_id], (err, row) =>{
    if (err){
      return console.log(err.message);
    }
    console.log("In get_value fun");
    console.log(row);
    return row;
  });

  db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Close the database connection.');
  });
}

function delete_value(order_id) {
  db = connect_db(db_name);
  sql = 'DELETE FROM customer where order_id=?'
  db.run(sql, order_id, (err)=> {
    if(err){
      return console.log(err);
    }
    console.log(`Row(s) deleted successfully`);
  });
  db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Close the database connection.');
  });
}


module.exports.select_all = select_all;
module.exports.insert_value = insert_value;
module.exports.get_value = get_value;
module.exports.delete_value = delete_value;