class MyCustomer{
    constructor(cust_id, name, order_id, description){
        this.cust_id = cust_id;
        this.name = name;
        this.order_id = order_id;
        this.description = description;
    }
}
module.exports = MyCustomer;